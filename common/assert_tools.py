
# 定义函数 - 实现 通用的 断言功能
def common_assert(resp, status_code, success, code, message):
    assert status_code == resp.status_code
    assert success is resp.json().get("success")
    assert code == resp.json().get("code")
    assert message in resp.json().get("message")

    # assert 200 == resp.status_code
    # assert True is resp.json().get("success")
    # assert 10000 == resp.json().get("code")
    # assert "操作成功" in resp.json().get("message")
