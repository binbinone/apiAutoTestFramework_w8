import requests


# 定义类 --- 登录接口
class IhrmLogin(object):
    # 定义方法
    def login(self, req_body):  # 形参
        resp = requests.post(url="http://ihrm2-test.itheima.net/api/sys/login",
                             json=req_body)
        return resp


# 必须 自测
if __name__ == '__main__':
    # 创建实例
    ihrm_login = IhrmLogin()
    # 准备请求体数据
    req_data = None
    # 使用实例调用 方法
    ret = ihrm_login.login(req_data)  # 实参
    # 打印查看 响应结果
    print("登录：", ret.json())
