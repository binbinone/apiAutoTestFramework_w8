import requests

# ihrm 登录
resp = requests.post(url="http://ihrm2-test.itheima.net/api/sys/login",
                     json={"mobile": "13800000002", "password": "123456"})
print("登录：", resp.json())

# ihrm 添加员工
resp = requests.post(url="http://ihrm2-test.itheima.net/api/sys/user",
                     headers={"Authorization": "b9c8f43a-c406-4af8-b2e8-0e1a04fd6ae3"},
                     json={
                         "username": "jack123",
                         "mobile": "18984893600",
                         "workNumber": "9527"
                     })
print("添加员工：", resp.json())

# ihrm 修改员工
resp = requests.put(url="http://ihrm2-test.itheima.net/api/sys/user/1650413846495506432",
                    headers={"Authorization": "b9c8f43a-c406-4af8-b2e8-0e1a04fd6ae3"},
                    json={"username": "刘大锤！"})
print("修改员工：", resp.json())

# ihrm 查询员工
resp = requests.get(url="http://ihrm2-test.itheima.net/api/sys/user/1650413846495506432",
                    headers={"Authorization": "b9c8f43a-c406-4af8-b2e8-0e1a04fd6ae3"})
print("查询员工：", resp.json())

# ihrm 删除员工
resp = requests.delete(url="http://ihrm2-test.itheima.net/api/sys/user/1650413846495506432",
                       headers={"Authorization": "b9c8f43a-c406-4af8-b2e8-0e1a04fd6ae3"})
print("删除员工：", resp.json())
