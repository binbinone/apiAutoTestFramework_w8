from api.emp_manage import IhrmEmpManage
from common.assert_tools import common_assert
from common.get_req_header import get_req_header


class TestAddEmp:
    """添加员工测试类"""

    def setup_class(self):
        # 创建 IhrmEmpManage 实例
        self.ihrm = IhrmEmpManage()

        # 调用自己封装的函数 获取请求头 - 1组够用！
        self.req_header = get_req_header()

    def test01_must_params(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": "18984893691",
            "workNumber": "9527"
        }
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加成功（必选）：", resp.json())
        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test02_all_params(self):
        # 准备请求体
        req_data = {
            "username": "唐伯虎7",
            "mobile": "18984893690",
            "formOfEmployment": 1,
            "workNumber": "9527",
            "departmentName": "测试某7部",
            "timeOfEntry": "2023-03-31",
            "correctionTime": "2023-04-29"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)

        # 打印 响应结果
        print("添加成功（全部）：", resp.json())

        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test03_mobile_exists(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": "18984893680",
            "workNumber": "9527"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加失败（手机号已存在）：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20002, "新增员工失败")

    def test04_username_none(self):
        # 准备请求体
        req_data = {
            "username": None,
            "mobile": "18984893680",
            "workNumber": "9527"
        }

        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("添加失败（用户名为空）：", resp.json())
        # 断言
        common_assert(resp, 200, False, 20002, "新增员工失败")

    def test05_more_params(self):
        # 准备请求体
        req_data = {
            "username": "jack123",
            "mobile": "18984893687",
            "workNumber": "9527",
            "abc": "123"
        }
        # 调用自己封装 添加员工 api, 得响应结果
        resp = self.ihrm.add_emp(self.req_header, req_data)
        # 打印 响应结果
        print("多参（多abc）：", resp.json())
        # 断言
        common_assert(resp, 200, True, 10000, "操作成功")

    def test06_less_params_dept_id(self):
        pass

    def test07_less_params(self):
        pass
