import pytest
from api.ihrm_login import IhrmInsert
from common.assert_tools import common_assert
from common.read_json_tools import read_json_data

"""
1. 读取json文件，获取 [（）,（）,（）]
2. 在 通用测试方法上一行，添加装饰器 @pytest.mark.parametrize()
3. 给 parametrize()添加参数。参1：1个字符串，内容为一组json数据的key， 参2：[（）,（）,（）]
4. 给 通用测试方法添加形参，就是 parametrize()参1字符串的内容。
5. 在 通用测试方法内，使用形参。
"""
data = read_json_data("login_data.json")


class TestInsertParams(object):
    """定义 测试类"""

    def setup_class(self):
        # 创建实例
        self.ihrm_insert = IhrmInsert()

    # 定义 通用 测试方法
    @pytest.mark.parametrize("desc, req_data, status_code, success, code, message", data)
    def test_login(self, desc, req_data, status_code, success, code, message):
        # 调用自己封装的 api，发送 登录接口请求
        resp = self.ihrm_insert.login(req_data)
        # 打印
        print(desc, "：", resp.json())
        # 断言
        common_assert(resp, status_code, success, code, message)